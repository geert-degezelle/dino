<?php
/**
 * Created by PhpStorm.
 * User: geertdegezelle
 * Date: 7/06/18
 * Time: 13:30
 */

namespace Drupal\dino\Controller;

use Symfony\Component\HttpFoundation\Response;


/**
 * Class NoiseController
 *
 * @package Drupal\dino\Controller
 */
class NoiseController {

  /**
   * Make some noise.
   */
  public function roar() {
    // Return something
    return new Response('Roar!');
  }
}